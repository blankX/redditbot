# redditbot

A reddit to telegram mirror thing because why not  

### Installation Instructions
1. Install:
    - `python3` (this is in python after all)
    - `ffmpeg` (to get the audio for videos)
2. `pip3 install -r requirements.txt`
3. Copy example_config.yaml to config.yaml and edit it

### Start
`python3 redditbot.py`  
